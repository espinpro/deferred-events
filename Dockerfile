# Build image
FROM golang:1.16-alpine3.13 as alpine_builder

RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/bitbucket.org/espinpro/deferred-events
COPY . .
# Actual build
RUN GOOS=linux GOARCH=amd64 go build -o /go/bin/deffered_events ./cmd/deferred_events

# Run image
FROM alpine:3.13

COPY --from=alpine_builder /go/bin/deffered_events /go/bin/deffered_events
RUN apk update && apk add --no-cache util-linux

# Run script
CMD [ "/go/bin/deffered_events" ]
