vendor:
	go mod tidy
	go mod vendor
	modvendor -copy="**/*.c **/*.h **/*.proto **/*.m" -v

init:
	chmod -R +x .githooks/
	mkdir -p .git/hooks/
	find .git/hooks -type l -exec rm {} \;
	find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

lint:
	golangci-lint run ./...

.PHONY: vendor init lint