package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"
	"sync"
	"time"

	"bitbucket.org/espinpro/deferred-events/embedded"

	"bitbucket.org/espinpro/go-engine/template"

	"bitbucket.org/espinpro/go-engine/utils"

	"bitbucket.org/espinpro/go-engine/registry/comparator"

	"bitbucket.org/espinpro/go-engine/fastlog"
	"bitbucket.org/espinpro/go-engine/registry/sortedset"
	"bitbucket.org/espinpro/go-engine/server"
	"bitbucket.org/espinpro/go-engine/server/middlewares"
	"github.com/google/gops/agent"
	"github.com/kelseyhightower/envconfig"
)

// Configuration constants
const (
	prefix          = "TIME_EVENTS"
	buffer          = 1000
	tickTimeout     = 100 * time.Millisecond
	dumpTickTimeout = 500 * time.Millisecond
	requestTimeout  = 3 * time.Second
)

// List of global variables
var (
	version        string
	listeners      = NewListeners()
	sortedSet      = sortedset.NewSortedSet(time.Time{}, comparator.TimeComparator)
	templateEngine = template.NewEngine()
)

// Config contains configs for server
type Config struct {
	GopsAddr           string        `split_words:"true" default:":6060"`
	Addr               string        `default:":8090"`
	ReadTimeout        time.Duration `split_words:"true" default:"10s"`
	WriteTimeout       time.Duration `split_words:"true" default:"10s"`
	DumpFile           string        `split_words:"true" default:"./dump.json"`
	ListenersDumpFile  string        `split_words:"true" default:"./listeners_dump.json"`
	DumpDelay          time.Duration `split_words:"true" default:"1s"`
	ListenersDumpDelay time.Duration `split_words:"true" default:"1s"`
	Documentation      string        `split_words:"true" default:"source/Readme.md"`
}

// GetConfigFromEnv return apt configs bases on environment variables
func GetConfigFromEnv() (*Config, error) {
	c := new(Config)
	err := envconfig.Process(prefix, c)
	return c, err
}

// RequestEntity describe entity from user request to store in sorted set
type RequestEntity struct {
	Key   string
	Value string
}

// Listener describe single listener
type Listener struct {
	ID        string
	TargetURL string
}

// Listeners contains all listeners
type Listeners struct {
	Data map[string]Listener `json:"data"`
	mu   sync.RWMutex
}

// NewListeners return listeners struct
func NewListeners() Listeners {
	return Listeners{
		Data: map[string]Listener{},
	}
}

// Add add listener to listeners
func (ls *Listeners) Add(l Listener) {
	ls.mu.Lock()
	defer ls.mu.Unlock()
	ls.Data[l.ID] = l
}

// Remove remove listener from listeners
func (ls *Listeners) Remove(id string) {
	ls.mu.Lock()
	defer ls.mu.Unlock()
	delete(ls.Data, id)
}

// List return list of listeners
func (ls *Listeners) List() []Listener {
	ls.mu.RLock()
	defer ls.mu.RUnlock()
	list := make([]Listener, len(ls.Data))
	var i int
	for _, l := range ls.Data {
		list[i] = l
		i++
	}
	return list
}

func main() {
	ctx := context.Background()
	fastlog.Infow("Running instance", "version", version)
	c, err := GetConfigFromEnv()
	if err != nil {
		fastlog.Errorw("Error getting config", "err", err)
	}

	err = DumpListener(ctx, c.DumpFile, func(dump []byte) error {
		return sortedSet.Restore(func(key string) (interface{}, error) {
			return time.Parse(time.RFC3339, key)
		}, string(dump))
	}, func() ([]byte, error) {
		d, err := sortedSet.Dump(func(key interface{}) (string, error) {
			t := key.(time.Time)
			return t.Format(time.RFC3339), nil
		})
		return []byte(d), err
	})
	if err != nil {
		fastlog.Errorw("Error restoring dump", "err", err, "dump", c.DumpFile)
		return
	}
	err = DumpListener(ctx, c.ListenersDumpFile, func(dump []byte) error {
		return json.Unmarshal(dump, &listeners)
	}, func() ([]byte, error) {
		return json.Marshal(&listeners)
	})
	if err != nil {
		fastlog.Errorw("Error restoring dump", "err", err, "dump", c.DumpFile)
		return
	}

	err = ListenListener(ctx)
	if err != nil {
		fastlog.Errorw("Error listen listeners", "err", err)
		return
	}

	tmpl, err := template.NewTemplateBySource(embedded.GetSource(), "main", "source/*.html")
	if err != nil {
		fastlog.Errorw("Error parsing folder", "err", err)
		return
	}
	templateEngine.Add(tmpl)

	router := server.Root()
	router.Add("/", server.NewRoute(http.MethodPost, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		e := new(RequestEntity)
		err = json.NewDecoder(r.Body).Decode(&e)
		if err != nil {
			fastlog.Errorw("Error decoding request", "err", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = r.Body.Close()
		if err != nil {
			fastlog.Errorw("Error closing body", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		key, err := time.Parse(time.RFC3339, e.Key)
		if err != nil {
			fastlog.Errorw("Error sending JSON response", "err", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		sortedSet.Upsert(key, e.Value)
		w.WriteHeader(http.StatusCreated)
	})), server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		res, err := templateEngine.Execute("main", nil)
		if err != nil {
			fastlog.Errorw("Error parsing response", "err", err)
			return
		}
		_, err = w.Write(res)
		if err != nil {
			fastlog.Errorw("Error sending data", "err", err)
			return
		}
	})))
	router.Add("/:value", server.NewRoute(http.MethodDelete, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		values := r.Context().Value(server.CtxValues).(map[string]string)
		sortedSet.Remove(values["value"])
		w.WriteHeader(http.StatusAccepted)
	})))
	router.Add("/until/:key/clear", server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		values := r.Context().Value(server.CtxValues).(map[string]string)
		key, err := time.Parse(time.RFC3339, values["key"])
		if err != nil {
			fastlog.Errorw("Error parsing key form path", "err", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		result := sortedSet.GetUntilKey(key, true)
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			fastlog.Errorw("Error sending JSON response", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})))
	router.Add("/until/:key", server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		values := r.Context().Value(server.CtxValues).(map[string]string)
		key, err := time.Parse(time.RFC3339, values["key"])
		if err != nil {
			fastlog.Errorw("Error parsing key form path", "err", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		result := sortedSet.GetUntilKey(key, false)
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			fastlog.Errorw("Error sending JSON response", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})))
	router.Add("/listener", server.NewRoute(http.MethodPost, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		e := new(Listener)
		err = json.NewDecoder(r.Body).Decode(&e)
		if err != nil {
			fastlog.Errorw("Error decoding request", "err", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = r.Body.Close()
		if err != nil {
			fastlog.Errorw("Error closing body", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		listeners.Add(*e)

		w.WriteHeader(http.StatusCreated)
	})), server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		result := listeners.List()
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(result)
		if err != nil {
			fastlog.Errorw("Error sending JSON response", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})))
	router.Add("/listener/:id", server.NewRoute(http.MethodDelete, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		values := r.Context().Value(server.CtxValues).(map[string]string)
		listeners.Remove(values["id"])

		w.WriteHeader(http.StatusAccepted)
	})))
	router.Add("/documentation", server.NewRoute(http.MethodGet, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		documentation, err := embedded.GetSource().ReadFile(c.Documentation)
		if err != nil {
			fastlog.Errorw("Error getting documentation", "documentation", c.Documentation, "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		_, err = w.Write(documentation)
		if err != nil {
			fastlog.Errorw("Error getting documentation", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})))
	router.Add("/s/*", server.NewRoute(http.MethodGet, http.StripPrefix("/s", http.FileServer(http.FS(embedded.GetSource())))))

	s := &http.Server{
		Addr:         c.Addr,
		Handler:      middlewares.Recover(router.Handler(nil)),
		ReadTimeout:  c.ReadTimeout,
		WriteTimeout: c.WriteTimeout,
	}
	s.RegisterOnShutdown(func() {
		err := s.Close()
		if err != nil {
			fastlog.Errorw("Error close server", "err", err)
		}
	})

	if err := agent.Listen(agent.Options{Addr: c.GopsAddr}); err != nil {
		fastlog.Errorw("Failed to run gops agent", "err", err)
	}

	if err := s.ListenAndServe(); err != nil {
		fastlog.Fatalw("Error listen server", "err", err)
	}
}

type Event struct {
	Data []byte
	URL  string
}

func DumpListener(ctx context.Context, filename string, restoreFn func(dump []byte) error, dumpFn func() ([]byte, error)) error {
	if filename == "" {
		return nil
	}
	dump, err := ioutil.ReadFile(filename)
	if err != nil {
		if perr, ok := err.(*os.PathError); ok && errors.Is(perr, os.ErrNotExist) {
			fastlog.Infow("Dump not exists")
		} else {
			return err
		}
	} else {
		fastlog.Infow("Restore dump", "filename", filename)
		err = restoreFn(dump)
		if err != nil {
			return err
		}
	}

	fastlog.Infow("Run dump creator", "filename", filename, "tick", dumpTickTimeout.String())
	go func() {
		ctx, cancel := context.WithCancel(ctx)
		tch := time.NewTicker(dumpTickTimeout)
		for {
			select {
			case <-ctx.Done():
				cancel()
				tch.Stop()
				return
			case <-tch.C:
				fastlog.Debugw("Create dump by tick", "filename", filename)

				dump, err := dumpFn()
				if err != nil {
					fastlog.Errorw("Error making dump", "err", err)
					continue
				}
				err = os.WriteFile(filename, dump, os.ModePerm)
				if err != nil {
					fastlog.Errorw("Error saving dump", "err", err)
					continue
				}
			}
		}
	}()
	return nil
}

func ListenListener(ctx context.Context) error {
	ctx, cancel := context.WithCancel(ctx)
	http.DefaultClient.Timeout = requestTimeout
	events := make(chan Event, buffer)
	go func() {
		tick := time.NewTicker(tickTimeout)
		for {
			select {
			case <-ctx.Done():
				cancel()
				return
			case <-tick.C:
				list := listeners.List()
				if len(list) == 0 {
					continue
				}
				keys := sortedSet.GetUntilKey(time.Now(), true)
				if len(keys) == 0 {
					continue
				}
				b, err := json.Marshal(keys)
				if err != nil {
					fastlog.Errorw("Error encoding keys", "err", err, "keys", keys)
					continue
				}
				for _, l := range list {
					if l.TargetURL == "" {
						continue
					}
					events <- Event{
						Data: b,
						URL:  l.TargetURL,
					}
				}
			}
		}
	}()
	go func() {
		utils.RunSyncMultipleWorkers(ctx, runtime.NumCPU(), func(ctx context.Context) {
			for {
				select {
				case <-ctx.Done():
					cancel()
					return
				case event := <-events:
					b := event.Data
					url := event.URL
					resp, err := http.Post(url, "application/json", bytes.NewReader(b)) //nolint:gosec
					if err != nil {
						fastlog.Errorw("Error sending request", "err", err, "keys", string(b))
						continue
					}
					err = resp.Body.Close()
					if err != nil {
						fastlog.Errorw("Error closing body", "err", err, "keys", string(b))
						continue
					}
				}
			}
		})
	}()
	return nil
}
