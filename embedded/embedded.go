package embedded

import (
	"embed"
)

//go:embed source/*
var source embed.FS

// GetSource return embed FS
func GetSource() embed.FS {
	return source
}
