# Deferred Events

"Deferred Events" is a service based on the `SortedSet` algorithm from the library [go-engine](https://bitbucket.org/espinpro/go-engine) . This service is created especially for the cover case when users have some data, and they should know when a specific date already happen. For example, users have a table with posts. Each post has a `publishedAt` field. If this field more of now, that means the post is not published yet. That service exactly helps to know when that post is published.
This service uses `Key` and `Value` to identify data. `Key` is the date in RFC3339, and `Value` is unique data associated with the `Key` (usually, this is some ID). It means the service creates a single record per `Value` and will update `Key` for the same `Value`.

"Deferred Events" is working over REST and use time in RFC3339 format.

## Listener
"Listener" the core feature of service. Provide functionality to subscribe on real-time updates. Every subscribed listener will receive all values with past keys.

## API

### POST on /

### Request

Create new data. Expect to receive JSON as request in the following format:

| name  | type   | description         |
| ----- | ------ | ------------------- |
| Key   | string | Time in RFC3339     |
| Value | any    | Unique value of key |

For example

```
{"Key":"2021-06-04T14:31:01+03:00", "Value": "abc"}
```

#### Response

On success return, HTTP status code `Created (201)` without the body.

On error return correspondent HTTP status code without body

### DELETE on /:value

#### Request

Delete data by `value` form path. Do not expect body.

#### Response

Always return HTTP status code `Accepted (202)` without a body.

### GET on /until/:date/[clear]

#### Request

Expect to receive `date` in path (in RFC3339), and optional parameter `clear`. If clear presented, will return data and remove them.

#### Response

On success, return HTTP status code `Success (200)` with JSON body. Body contains list of all past `Key` values.

On error return correspondent HTTP status code without body

### POST on /listener

### Request

Create a new data listener. Expect to receive JSON as request in the following format:

| name      | type   | description    |
| --------- | ------ | -------------- |
| ID        | string | ID of listener |
| TargetURL | string | URL            |

For example

```
{"ID": "test", "TargetURL": "https://echo.espin.pro/"}
```

### GET on /listener

### Request

#### Response

On success, return HTTP status code `Success (200)` with JSON body. Body contains list of all listeners.

On error return correspondent HTTP status code without body

### DELETE on /listener/:id

### Request

Remove listener by given `id` in the path.

#### Response

Always return HTTP status code `Accepted (202)` without a body.

### GET on /documentation

#### Request

#### Response

Return this markdown
